Studentnummer: 329892
Naam: Ouassim Boukayoua

Wat is Git?
Git is een type versie controle systeem (VCS) dat het gemakkelijker maakt om wijzigingen in bestanden bij te houden.

Waar kan je een repository voor gebruiken?
Een repository bemiddelt tussen het domein en de datamapping-lagen van uw applicatie. Het zou u een inkapseling moeten geven over de manier waarop gegevens daadwerkelijk worden bewaard in de gegevensopslaglaag.

Wat kan je met een commit?
Het git commit commando zal alle veranderingen, samen met een korte beschrijving van de gebruiker, opslaan in een "commit" naar de lokale repository. Commits vormen de kern van het gebruik van Git. Je kunt een commit zien als een momentopname van je project, waarbij een nieuwe versie van dat project wordt aangemaakt in de huidige repository.

Zoek de volgende commando's op:
 - Creëren van een repository
 - Bestanden toevoegen aan staging area
 - Bestanden committen 









