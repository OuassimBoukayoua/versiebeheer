Studentnummer:
Naam:Ouassim Boukayoua

Hoe open je de Terminal in Visual Studio Code?
	#CTRL+Shift+` of helemaal boven in op het knopje terminal te drukken en dan new terminal.

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	#git status

Wat is het commando van een multiline commit message?
	#git commit -m "Title of commit" -m "rest of message 
	>> new line of message"

Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	#9

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	#git reset HEAD~1
